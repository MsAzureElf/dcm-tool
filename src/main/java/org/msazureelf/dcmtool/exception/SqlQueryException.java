package org.msazureelf.dcmtool.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

/**
 * @author <a href="mailto:helen.militsyna@gmail.com">Elena Militsyna<a/>
 * created on 7/28/2019
 */

@ResponseStatus(value = HttpStatus.BAD_REQUEST, reason = "SQL_QUERY_EXCEPTION")
public class SqlQueryException extends RuntimeException {

    public SqlQueryException(String msg) {
        super(msg);
    }
}
