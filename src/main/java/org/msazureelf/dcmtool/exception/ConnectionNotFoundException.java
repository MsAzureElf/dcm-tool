package org.msazureelf.dcmtool.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

/**
 * @author <a href="mailto:helen.militsyna@gmail.com">Elena Militsyna<a/>
 * created on 7/27/2019
 */

@ResponseStatus(value = HttpStatus.NOT_FOUND, reason = "CONNECTION_NOT_FOUND")
public class ConnectionNotFoundException extends RuntimeException {

    public ConnectionNotFoundException(String msg) {
        super(msg);
    }
}
