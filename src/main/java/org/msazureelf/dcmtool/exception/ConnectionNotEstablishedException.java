package org.msazureelf.dcmtool.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

/**
 * @author <a href="mailto:helen.militsyna@gmail.com">Elena Militsyna<a/>
 * created on 7/28/2019
 */

@ResponseStatus(value = HttpStatus.NOT_ACCEPTABLE, reason = "CONNECTION_NOT_ESTABLISHED")
public class ConnectionNotEstablishedException extends RuntimeException {

    public ConnectionNotEstablishedException(String msg) {
        super(msg);
    }
}
