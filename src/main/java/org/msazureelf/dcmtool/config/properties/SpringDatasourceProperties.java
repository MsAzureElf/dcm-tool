package org.msazureelf.dcmtool.config.properties;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

/**
 * @author <a href="mailto:helen.militsyna@gmail.com">Elena Militsyna<a/>
 * created on 7/28/2019
 */

@ConfigurationProperties(prefix = "spring.datasource")
@Data
@Configuration
public class SpringDatasourceProperties {

    private String driverClassName;

    private String url;

    private String username;

    private String password;

}
