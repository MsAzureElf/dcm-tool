package org.msazureelf.dcmtool.model.jpa;

import lombok.Data;

import javax.persistence.*;
import javax.validation.constraints.NotNull;

/**
 * @author <a href="mailto:helen.militsyna@gmail.com">Elena Militsyna<a/>
 * created on 7/27/2019
 */

@Entity
@Data
@Table(name = "connections")
public class DatabaseConnection {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @NotNull
    @Column(nullable = false)
    private String name;

    @NotNull
    @Column(nullable = false)
    private String hostname;

    @NotNull
    @Column(nullable = false)
    private Integer port;

    @NotNull
    @Column(nullable = false)
    private String databaseName;

    @NotNull
    @Column(nullable = false)
    private String username;

    @NotNull
    @Column(nullable = false)
    private String password;

    public String toUrlConnectionString(){
        return String.format("jdbc:postgresql://%s:%s/%s", hostname, port, databaseName);
    }

}
