package org.msazureelf.dcmtool.model.mapper;

import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;
import org.msazureelf.dcmtool.model.dto.DatabaseConnectionDTO;
import org.msazureelf.dcmtool.model.jpa.DatabaseConnection;

/**
 * @author <a href="mailto:helen.militsyna@gmail.com">Elena Militsyna<a/>
 * created on 7/27/2019
 */

@Mapper
public interface DatabaseConnectionMapper {

    DatabaseConnectionMapper instance = Mappers.getMapper(DatabaseConnectionMapper.class);

    DatabaseConnectionDTO connectionToConnectionDTO(DatabaseConnection connection);

    DatabaseConnection connectionDTOToConnection(DatabaseConnectionDTO connection);

}
