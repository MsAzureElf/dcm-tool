package org.msazureelf.dcmtool.model.dto;

import lombok.Data;

import java.io.Serializable;

/**
 * @author <a href="mailto:helen.militsyna@gmail.com">Elena Militsyna<a/>
 * created on 7/27/2019
 */

@Data
public class DatabaseConnectionDTO implements Serializable {

    private Long id;

    private String name;

    private String hostname;

    private Integer port;

    private String databaseName;

    private String username;

    private String password;

}
