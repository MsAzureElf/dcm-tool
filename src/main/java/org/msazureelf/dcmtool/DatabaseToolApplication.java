package org.msazureelf.dcmtool;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @author <a href="mailto:helen.militsyna@gmail.com">Elena Militsyna<a/>
 * created on 7/27/2019
 */

@SpringBootApplication
public class DatabaseToolApplication {

	public static void main(String[] args) {
		SpringApplication.run(DatabaseToolApplication.class, args);
	}

}
