package org.msazureelf.dcmtool.service;

import java.util.List;
import java.util.Map;

public interface QueriesService {

    List<Map<String, String>> getSchemaList(Long connectionId);

    List<Map<String, String>> getTableList(Long connectionId, String schemaName);

    List<Map<String, String>> getColumnList(Long connectionId, String schemaName, String tableName);

    List<Map<String, String>> getTablePreview(Long connectionId, String schemaName, String tableName, Integer limit);

    List<Map<String, String>> getColumnsStatistics(Long connectionId, String schemaName, String tableName);

    List<Map<String, String>> getTablesStatistics(Long connectionId, String schemaName);

}
