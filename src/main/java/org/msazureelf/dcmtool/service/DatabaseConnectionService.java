package org.msazureelf.dcmtool.service;

import org.msazureelf.dcmtool.model.dto.DatabaseConnectionDTO;

import java.util.List;

/**
 * @author <a href="mailto:helen.militsyna@gmail.com">Elena Militsyna<a/>
 * created on 7/27/2019
 */

public interface DatabaseConnectionService {

    void saveDatabaseConnection(DatabaseConnectionDTO connection);

    void deleteDatabaseConnection(Long connectionId);

    List<DatabaseConnectionDTO> getAllConnections();

    DatabaseConnectionDTO getConnection(Long connectionId);

}
