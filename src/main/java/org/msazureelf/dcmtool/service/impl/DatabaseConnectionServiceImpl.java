package org.msazureelf.dcmtool.service.impl;

import lombok.RequiredArgsConstructor;
import org.msazureelf.dcmtool.exception.ConnectionNotFoundException;
import org.msazureelf.dcmtool.model.dto.DatabaseConnectionDTO;
import org.msazureelf.dcmtool.model.jpa.DatabaseConnection;
import org.msazureelf.dcmtool.model.mapper.DatabaseConnectionMapper;
import org.msazureelf.dcmtool.repository.DatabaseConnectionRepository;
import org.msazureelf.dcmtool.service.DatabaseConnectionService;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

/**
 * @author <a href="mailto:helen.militsyna@gmail.com">Elena Militsyna<a/>
 * created on 7/27/2019
 */

@Service
@RequiredArgsConstructor
public class DatabaseConnectionServiceImpl implements DatabaseConnectionService {

    private final DatabaseConnectionRepository repository;

    @Override
    public void saveDatabaseConnection(DatabaseConnectionDTO connection) {
        Long id = connection.getId();
        if (id == null) {
            repository.save(convertConnectionDTOToEntity(connection));
        } else {
            repository
                    .findById(id)
                    .orElseThrow(() -> new ConnectionNotFoundException(String.format("Connection with id %d was not found", id)));
            repository.save(convertConnectionDTOToEntity(connection));
        }
    }

    @Override
    public void deleteDatabaseConnection(Long connectionId) {
        repository
                .findById(connectionId)
                .orElseThrow(() -> new ConnectionNotFoundException(String.format("Connection with id %d was not found", connectionId)));
        repository.deleteById(connectionId);
    }

    @Override
    public DatabaseConnectionDTO getConnection(Long connectionId) {
        return convertConnectionEntityToDTO(repository
                .findById(connectionId)
                .orElseThrow(() -> new ConnectionNotFoundException(String.format("Connection with id %d was not found", connectionId))));
    }

    @Override
    public List<DatabaseConnectionDTO> getAllConnections() {
        return repository.findAll()
                .stream()
                .map(this::convertConnectionEntityToDTO)
                .collect(Collectors.toList());
    }

    private DatabaseConnectionDTO convertConnectionEntityToDTO(DatabaseConnection connection) {
        return DatabaseConnectionMapper.instance.connectionToConnectionDTO(connection);
    }

    private DatabaseConnection convertConnectionDTOToEntity(DatabaseConnectionDTO connection) {
        return DatabaseConnectionMapper.instance.connectionDTOToConnection(connection);
    }

}
