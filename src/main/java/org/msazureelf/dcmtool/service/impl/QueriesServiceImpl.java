package org.msazureelf.dcmtool.service.impl;

import lombok.RequiredArgsConstructor;
import org.msazureelf.dcmtool.config.properties.SpringDatasourceProperties;
import org.msazureelf.dcmtool.exception.ConnectionNotEstablishedException;
import org.msazureelf.dcmtool.exception.ConnectionNotFoundException;
import org.msazureelf.dcmtool.exception.SqlQueryException;
import org.msazureelf.dcmtool.model.jpa.DatabaseConnection;
import org.msazureelf.dcmtool.repository.DatabaseConnectionRepository;
import org.msazureelf.dcmtool.service.QueriesService;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.datasource.DriverManagerDataSource;
import org.springframework.jdbc.datasource.SingleConnectionDataSource;
import org.springframework.stereotype.Service;

import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

/**
 * @author <a href="mailto:helen.militsyna@gmail.com">Elena Militsyna<a/>
 * created on 7/28/2019
 */

@Service
@RequiredArgsConstructor
public class QueriesServiceImpl implements QueriesService {

    private final DatabaseConnectionRepository repository;

    private final SpringDatasourceProperties datasourceProperties;

    @Override
    public List<Map<String, String>> getSchemaList(Long connectionId) throws ConnectionNotFoundException, ConnectionNotEstablishedException {
        return getQueryData(connectionId, "SELECT schema_name FROM information_schema.schemata ORDER BY schema_name");
    }

    @Override
    public List<Map<String, String>> getTableList(Long connectionId, String schemaName) {
        return getQueryData(connectionId, String.format("SELECT * FROM information_schema.tables WHERE table_schema = '%s'", schemaName));
    }

    @Override
    public List<Map<String, String>> getColumnList(Long connectionId, String schemaName, String tableName) {
        return getQueryData(connectionId, String.format("SELECT * FROM information_schema.columns WHERE table_schema = '%s' AND table_name = '%s'", schemaName, tableName));
    }

    @Override
    public List<Map<String, String>> getTablePreview(Long connectionId, String schemaName, String tableName, Integer limit) {
        return getQueryData(connectionId, String.format("SELECT * FROM %s.%s LIMIT %d", schemaName, tableName, Math.max(0, limit)));
    }

    @Override
    public List<Map<String, String>> getColumnsStatistics(Long connectionId, String schemaName, String tableName) {
        //TODO: extract all SQL templates to yaml file
        List<Map<String, String>> columnNamesMap = getQueryData(connectionId, String.format("\n" +
                "SELECT column_name FROM information_schema.columns \n" +
                "\tWHERE table_schema = '%s' AND table_name = '%s'\n" +
                "\tAND data_type IN ('smallint', 'integer', 'bigint', 'decimal', 'numeric', 'real', 'double precision', 'smallserial', 'serial', 'bigserial', 'money')", schemaName, tableName));
        return columnNamesMap.stream().map(v -> {
            String columnName = v.get("column_name");
            return getQueryData(connectionId, String.format(
                    "SELECT '%1$s' AS column_name, min(%1$s), max(%1$s), avg(%1$s), percentile_cont(0.5) WITHIN GROUP (ORDER BY %2$s.%1$s) AS median FROM %3$s.%2$s", columnName, tableName, schemaName)).get(0);
        }).collect(Collectors.toList());
    }

    @Override
    public List<Map<String, String>> getTablesStatistics(Long connectionId, String schemaName) {
        List<Map<String, String>> tablesList = getQueryData(connectionId, String.format("SELECT table_name FROM information_schema.tables WHERE table_schema = '%s'", schemaName));
        return tablesList.stream().map(v -> {
            String tableName = v.get("table_name");
            return getQueryData(connectionId, String.format("\n" +
                    "SELECT '%1$s' as table_name, rows_count, columns_count\n" +
                    "FROM (SELECT count(*) AS rows_count from %1$s) row_counts,\n" +
                    "(SELECT count(distinct column_name) AS columns_count FROM information_schema.columns WHERE table_schema = '%2$s' AND table_name = '%1$s') column_counts", tableName, schemaName)).get(0);
        }).collect(Collectors.toList());
    }

    private List<Map<String, String>> getQueryData(Long connectionId, String query){
        DatabaseConnection dbConnection = repository
                .findById(connectionId)
                .orElseThrow(() -> new ConnectionNotFoundException(String.format("Connection with id %d was not found", connectionId)));
            try (Connection connection = getConnection(dbConnection).getConnection()) {
                return new JdbcTemplate(new SingleConnectionDataSource(connection, true))
                        .query(query, (rs, rowNum) -> getRowData(rs));
            } catch (SQLException e){
                throw new ConnectionNotEstablishedException(e.getMessage());
            }
    }

    private DataSource getConnection(DatabaseConnection dbConnection) {
        DriverManagerDataSource dataSource = new DriverManagerDataSource();
        dataSource.setDriverClassName(datasourceProperties.getDriverClassName());
        dataSource.setUrl(dbConnection.toUrlConnectionString());
        dataSource.setUsername(dbConnection.getUsername());
        dataSource.setPassword(dbConnection.getPassword());
        return dataSource;
    }

    private Map<String, String> getRowData(ResultSet rs) throws SqlQueryException {
        try {
            Map<String, String> map = new HashMap<>();
            ResultSetMetaData rsmd = rs.getMetaData();
            IntStream.range(0, rsmd.getColumnCount()).forEach(n -> {
                try {
                    map.put(rsmd.getColumnName(n+1), rs.getString(n+1));
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            });
            return map;
        } catch (SQLException e){
            throw new SqlQueryException("Error occured while performing database query");
        }
    }
}
