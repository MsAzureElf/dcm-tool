package org.msazureelf.dcmtool.controller;

import lombok.RequiredArgsConstructor;
import org.msazureelf.dcmtool.model.dto.DatabaseConnectionDTO;
import org.msazureelf.dcmtool.service.DatabaseConnectionService;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * @author <a href="mailto:helen.militsyna@gmail.com">Elena Militsyna<a/>
 * created on 7/27/2019
 */

@RequiredArgsConstructor
@RestController
public class DatabaseConnectionController {

    private final DatabaseConnectionService service;

    @PostMapping("/connections")
    public void saveConnection(@RequestBody DatabaseConnectionDTO newConnection) {
        service.saveDatabaseConnection(newConnection);
    }

    @GetMapping("/connections/{id}")
    public DatabaseConnectionDTO getConnection(@PathVariable("id") Long connectionId) {
        return service.getConnection(connectionId);
    }

    @GetMapping("/connections")
    public List<DatabaseConnectionDTO> getConnections() {
        return service.getAllConnections();
    }

    @DeleteMapping("/connections/{id}")
    public void deleteConnection(@PathVariable("id") Long connectionId) {
        service.deleteDatabaseConnection(connectionId);
    }

}
