package org.msazureelf.dcmtool.controller;

import lombok.RequiredArgsConstructor;
import org.msazureelf.dcmtool.service.QueriesService;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.Map;

/**
 * @author <a href="mailto:helen.militsyna@gmail.com">Elena Militsyna<a/>
 * created on 7/28/2019
 */

@RequiredArgsConstructor
@RestController
public class QueriesController {

    private final QueriesService service;

    @GetMapping("/metadata/schemas")
    public List<Map<String, String>> getSchemaList(@RequestParam(value = "connectionId") Long connectionId) {
        return service.getSchemaList(connectionId);
    }

    @GetMapping("/metadata/tables")
    public List<Map<String, String>> getTableList(@RequestParam(value = "connectionId") Long connectionId,
                                                  @RequestParam(value = "schema") String schemaName) {
        return service.getTableList(connectionId, schemaName);
    }

    @GetMapping("/metadata/columns")
    public List<Map<String, String>> getColumnList(@RequestParam(value = "connectionId") Long connectionId,
                                                   @RequestParam(value = "schema") String schemaName,
                                                   @RequestParam(value = "table") String tableName) {
        return service.getColumnList(connectionId, schemaName, tableName);
    }

    @GetMapping("/table-data")
    public List<Map<String, String>> getTablePreview(@RequestParam(value = "connectionId") Long connectionId,
                                                     @RequestParam(value = "schema") String schemaName,
                                                     @RequestParam(value = "table") String tableName,
                                                     @RequestParam(value = "limit", defaultValue = "100") Integer limit) {
        return service.getTablePreview(connectionId, schemaName, tableName, limit);
    }

}
