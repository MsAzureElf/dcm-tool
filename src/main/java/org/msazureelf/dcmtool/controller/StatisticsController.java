package org.msazureelf.dcmtool.controller;

import lombok.RequiredArgsConstructor;
import org.msazureelf.dcmtool.service.QueriesService;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.Map;

/**
 * @author <a href="mailto:helen.militsyna@gmail.com">Elena Militsyna<a/>
 * created on 7/29/2019
 */

@RequiredArgsConstructor
@RestController
public class StatisticsController {

    private final QueriesService service;

    @GetMapping("/statistics/columns")
    public List<Map<String, String>> getColumnsStatistics(@RequestParam(value = "connectionId") Long connectionId,
                                                          @RequestParam(value = "schema") String schemaName,
                                                          @RequestParam(value = "table") String tableName) {
        return service.getColumnsStatistics(connectionId, schemaName, tableName);
    }

    @GetMapping("/statistics/tables")
    public List<Map<String, String>> getTablesStatistics(@RequestParam(value = "connectionId") Long connectionId,
                                                         @RequestParam(value = "schema") String schemaName) {
        return service.getTablesStatistics(connectionId, schemaName);
    }

}
