package org.msazureelf.dcmtool.repository;

import org.msazureelf.dcmtool.model.jpa.DatabaseConnection;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * @author <a href="mailto:helen.militsyna@gmail.com">Elena Militsyna<a/>
 * created on 7/27/2019
 */

@Repository
public interface DatabaseConnectionRepository extends JpaRepository<DatabaseConnection, Long> {

}
