# Database Connection Management Tool (dcm-tool)

## Overview

By default application starts on port 8080 and connects to PostgreSQL database `dcmtool` running on `localhost:5432`.

Database connection and server port can be changed in `application.properties` file.

Application supports connecting to PostgreSQL database instances. Version of PostgreSQL should be *9.4 or higher*.

## Supported REST requests

```
Note: in examples hereinafter it is assumed that server runs on localhost:8080
```

### Management of database connections, persisted in own PostgreSQL database specified in application properties

* **Add/update db connections:** `POST localhost:8080/connections`

To add a new connection, in body of request please specify connection attributes in JSON format:
```
    {
    	"name":"polarion-upd",
    	"hostname":"localhost",
    	"port":"5432",
    	"databaseName":"dcmtool",
    	"username":"postgres",
    	"password":"postgres"
    }
```
To modify an existing connection, in body of request specify connection attributes in JSON format as in the add request plus the id of the connection which you will be updating:
```
    {
    	"id":"1",
    	"name":"polarion-upd",
    	"hostname":"localhost",
    	"port":"5432",
    	"databaseName":"dcmtool",
    	"username":"postgres",
    	"password":"postgres"
    }
```

* **List db connections**

Use `GET localhost:8080/connections` to get the details of all the persisted db connections in JSON format

Use `GET localhost:8080/connections/{id}` to get details of one specific db connection. Put your desired id instead of `{id}` in example above.

* **Delete db connection**

Use `DELETE localhost:8080/connections/{id}` to delete one specific db connection from the persistence. Put your desired id instead of `{id}` in example above.

### Performing requests to databases connected from persisted connections

* **Get schema list from database*

Use `localhost:8080/metadata/schemas?connectionId=1` to get list of all schemas with their attributes
for the database connection with `connectionId`

* **Get table list from database and schema**

Use `GET localhost:8080/metadata/tables?connectionId=1&schema=public` to get list of all tables with their attributes
for the database connection with `connectionId` and schema `schema`

* **Get columns list from specified database, schema and table** 

Use `GET localhost:8080/metadata/columns?connectionId=1&schema=public&table=connections` to get list of all columns with their attributes for the database connection with `connectionId` and table `schema`.`table`

* **Get preview data from database, schema and table**

Use `GET localhost:8080/table-data?connectionId=1&schema=public&table=connections` to get the preview of table in `schema` and for `connectionId`

### Get statistics for columns and tables

* **Get statistics for columns in a specified table**

Example request:

```
localhost:8080/statistics/columns?connectionId=1&schema=public&table=prices
```

Example response:
```
[
    {
        "min": "1",
        "avg": "2.5000000000000000",
        "median": "2.5",
        "max": "4",
        "column_name": "id"
    },
    {
        "min": "1",
        "avg": "3.5",
        "median": "1.5",
        "max": "10",
        "column_name": "price"
    }
]
```

* **Get statistics for tables in a specified schema**

Example request:

```
localhost:8080/statistics/tables?connectionId=1&schema=public
```

Example response:
```
[
    {
        "rows_count": "1",
        "table_name": "databasechangeloglock",
        "columns_count": "4"
    },
    {
        "rows_count": "1",
        "table_name": "databasechangelog",
        "columns_count": "14"
    },
    {
        "rows_count": "3",
        "table_name": "connections",
        "columns_count": "7"
    },
    {
        "rows_count": "4",
        "table_name": "prices",
        "columns_count": "3"
    }
]
```
